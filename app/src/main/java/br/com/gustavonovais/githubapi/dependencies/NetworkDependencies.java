package br.com.gustavonovais.githubapi.dependencies;

import javax.inject.Singleton;

import br.com.gustavonovais.githubapi.detail.DetailFragment;
import br.com.gustavonovais.githubapi.home.ContentFragment;
import br.com.gustavonovais.githubapi.networking.NetworkModule;
import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface NetworkDependencies {
    void injectContent(ContentFragment contentFragment);
    void injectDetail(DetailFragment contentFragment);
}
