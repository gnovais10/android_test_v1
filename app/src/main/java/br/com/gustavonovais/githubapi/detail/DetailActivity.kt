package br.com.gustavonovais.githubapi.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import br.com.gustavonovais.githubapi.R
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    companion object {
        const val PARAM_URL = "url_pull"
    }

    private lateinit var url : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        url = intent.extras.getString(PARAM_URL)

        initFragment(DetailFragment.Companion.newInstance(url))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initFragment(fragment: DetailFragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frame, fragment)
        transaction.commit()
    }

}
