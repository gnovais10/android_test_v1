package br.com.gustavonovais.githubapi.home.adapter

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gustavonovais.githubapi.R
import br.com.gustavonovais.githubapi.models.DetailResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_item.view.*
import java.util.*


/**
 * Created by gustavon on 05/03/18.
 */
class DetailAdapter(private var listItem: ArrayList<DetailResponse>): RecyclerView.Adapter<DetailAdapter.ViewHolder>() {

    lateinit var activity : AppCompatActivity


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item  = listItem.get(position)

        holder?.let {
            holder.descRepo.text = item.body
            holder.title.text = item.title
            holder.name.text = item.user.login
            holder.date.text = item.created_at

            Picasso.with(activity).load(item.user.avatar_url).placeholder(R.drawable.placeholder).into(holder.imageAuthor)

            holder.card_content.setOnClickListener{
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(item.html_url)
                activity.startActivity(i)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.detail_item, parent, false)
        activity = parent?.context as AppCompatActivity
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val card_content = itemView.card_content
        val descRepo = itemView.desc_repo
        val title = itemView.title
        val imageAuthor = itemView.image_author
        val name = itemView.name_author
        val date = itemView.date

    }

}