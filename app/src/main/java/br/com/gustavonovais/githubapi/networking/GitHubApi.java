package br.com.gustavonovais.githubapi.networking;


import java.util.ArrayList;

import br.com.gustavonovais.githubapi.models.DetailResponse;
import br.com.gustavonovais.githubapi.models.RepoResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface GitHubApi {

    @GET
    Observable<RepoResponse.DataRepoResponse> getRepo(@Url String url, @Query("sort") String sort, @Query("page") String page);

    @GET
    Observable<ArrayList<DetailResponse>> getPull(@Url String url);
}
