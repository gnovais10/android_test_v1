package br.com.gustavonovais.githubapi.networking;

import java.util.ArrayList;

import br.com.gustavonovais.githubapi.BuildConfig;
import br.com.gustavonovais.githubapi.models.DetailResponse;
import br.com.gustavonovais.githubapi.models.RepoResponse;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static br.com.gustavonovais.githubapi.BuildConfig.URL_REPO;

public class Service {
    private final GitHubApi gitHubApi;

    public Service(GitHubApi gitHubApi) {
        this.gitHubApi = gitHubApi;
    }

    public Subscription getRepo(final CallbackRepo callbackRepo, final String lang, final String page) {
        return gitHubApi.getRepo(URL_REPO + lang, BuildConfig.SORT_BY, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends RepoResponse.DataRepoResponse>>() {
                    @Override
                    public Observable<? extends RepoResponse.DataRepoResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<RepoResponse.DataRepoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callbackRepo.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(RepoResponse.DataRepoResponse dataRepoResponse) {
                        callbackRepo.onSuccess(dataRepoResponse);
                    }
                });
    }

    public Subscription getPull(final CallbackPull callbackPull, final String url) {
        return gitHubApi.getPull(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ArrayList<DetailResponse>>>() {
                    @Override
                    public Observable<? extends ArrayList<DetailResponse>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ArrayList<DetailResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callbackPull.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(ArrayList<DetailResponse> detailResponses) {
                        callbackPull.onSuccess(detailResponses);
                    }
                });

    }

    public interface CallbackRepo {
        void onSuccess(RepoResponse.DataRepoResponse cityListResponse);
        void onError(NetworkError networkError);
    }

    public interface CallbackPull {
        void onSuccess(ArrayList<DetailResponse> detailResponse);
        void onError(NetworkError networkError);
    }
}
