package br.com.gustavonovais.githubapi.detail

import br.com.gustavonovais.githubapi.models.DetailResponse

/**
 * Created by gustavon on 06/03/18.
 */
interface DetailView {
    fun getDetailSucess(dataRepoResponse: ArrayList<DetailResponse>)
    fun getDetailError()
}