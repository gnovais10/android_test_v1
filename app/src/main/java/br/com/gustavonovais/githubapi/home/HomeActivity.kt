package br.com.gustavonovais.githubapi.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.gustavonovais.githubapi.R
import kotlinx.android.synthetic.main.activity_home.*

open class HomeActivity : AppCompatActivity(){

    companion object {
        const val JAVA: String = "java"
        const val NODE: String = "node"
        const val JAVA_SCRIPT: String = "javascript"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initFragment(ContentFragment.newInstance(JAVA))
        onNavigation()
    }

    private fun onNavigation() {
        navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> {
                    changeFragment(ContentFragment.newInstance(JAVA), JAVA)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    changeFragment(ContentFragment.newInstance(NODE), NODE)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_notifications -> {
                    changeFragment(ContentFragment.newInstance(JAVA_SCRIPT), JAVA_SCRIPT)
                    return@setOnNavigationItemSelectedListener true
                }
            }
            false
        }
    }

    private fun initFragment(fragment: ContentFragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frame, fragment)
        transaction.commit()
    }

    private fun changeFragment(fragment: ContentFragment, repo : String){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame, fragment)
        transaction.commit()
    }
}
