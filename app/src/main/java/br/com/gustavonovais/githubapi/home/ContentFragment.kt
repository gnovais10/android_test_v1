package br.com.gustavonovais.githubapi.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.gustavonovais.githubapi.BuildConfig
import br.com.gustavonovais.githubapi.R
import br.com.gustavonovais.githubapi.dependencies.DaggerNetworkDependencies
import br.com.gustavonovais.githubapi.home.HomeActivity.Companion.JAVA
import br.com.gustavonovais.githubapi.home.adapter.ContentAdapter
import br.com.gustavonovais.githubapi.models.RepoResponse
import br.com.gustavonovais.githubapi.networking.NetworkModule
import br.com.gustavonovais.githubapi.networking.Service
import kotlinx.android.synthetic.main.fragment_content.*
import javax.inject.Inject


class ContentFragment : Fragment(), HomeView, ContentAdapter.LastItemInterface {
    private var repo: String = "repo"
    private var isPagination = false
    private var sizeAdapter: Int = 0
    private var listDataRepoResponse = mutableListOf<RepoResponse.Item>()
    private var pageSize: Int = 1

    @Inject
    lateinit var service: Service

    private lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            repo = arguments.getString(Companion.PARAM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        DaggerNetworkDependencies.builder().networkModule(NetworkModule()).build().injectContent(this)
        presenter = HomePresenter(service, this)
        managerContentFragment()
    }


    private fun managerContentFragment() {
        load.visibility = View.VISIBLE

        when (repo) {
            (HomeActivity.Companion.JAVA) -> presenter.getRepo(HomeActivity.JAVA, BuildConfig.PAGE_INIT)
            (HomeActivity.Companion.JAVA_SCRIPT) -> presenter.getRepo(HomeActivity.JAVA_SCRIPT, BuildConfig.PAGE_INIT)
            (HomeActivity.Companion.NODE) -> presenter.getRepo(HomeActivity.NODE, BuildConfig.PAGE_INIT)
        }
    }

    override fun getRepoSucess(dataRepoResponse: RepoResponse.DataRepoResponse) {
        if (load.visibility == View.VISIBLE){
            load.visibility = View.GONE
        }
        recycler_repo.layoutManager = LinearLayoutManager(context)

        if (isPagination) {
            recycler_repo.adapter = ContentAdapter(listDataRepoResponse as ArrayList<RepoResponse.Item>, this)
            recycler_repo.scrollToPosition(sizeAdapter)
            recycler_repo.adapter.notifyDataSetChanged()
        } else {
            recycler_repo.adapter = ContentAdapter(dataRepoResponse.items as ArrayList<RepoResponse.Item>, this)
        }
        listDataRepoResponse.addAll(dataRepoResponse.items)
    }

    override fun getRepoError() {
        if (load.visibility == View.VISIBLE){
            load.visibility = View.GONE
        }
        Toast.makeText(this.activity, getString(R.string.error_message), Toast.LENGTH_SHORT).show()
    }

    override fun onLastItem(page: String, sizeList: Int) {
        sizeAdapter = sizeList
        isPagination = true
        pageSize++
        presenter.getRepo(JAVA, pageSize.toString())
    }

    companion object {
        const val PARAM = "param"

        fun newInstance(repo: String): ContentFragment {
            val fragment = ContentFragment()
            val args = Bundle()
            args.putString(PARAM, repo)
            fragment.arguments = args
            return fragment
        }
    }

}
