package br.com.gustavonovais.githubapi.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import br.com.gustavonovais.githubapi.R
import br.com.gustavonovais.githubapi.dependencies.DaggerNetworkDependencies
import br.com.gustavonovais.githubapi.home.ContentFragment
import br.com.gustavonovais.githubapi.home.adapter.DetailAdapter
import br.com.gustavonovais.githubapi.models.DetailResponse
import br.com.gustavonovais.githubapi.networking.NetworkModule
import br.com.gustavonovais.githubapi.networking.Service
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

/**
 * Created by gustavon on 06/03/18.
 */
class DetailFragment : Fragment(), DetailView {

    @Inject
    lateinit var service: Service
    lateinit var presenter: DetailPresenter

    lateinit var url : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            url = arguments.getString(DetailFragment.PARAM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        DaggerNetworkDependencies.builder().networkModule(NetworkModule()).build().injectDetail(this)

        presenter = DetailPresenter(service, this)

        load.visibility = View.VISIBLE
        presenter.getPulls(url)

    }

    override fun getDetailSucess(dataRepoResponse: ArrayList<DetailResponse>) {
        if (load.visibility == View.VISIBLE){
            load.visibility = View.GONE
        }

        recycler_detail.layoutManager = LinearLayoutManager(context)
        recycler_detail.adapter = DetailAdapter(dataRepoResponse)
    }

    override fun getDetailError() {
        Toast.makeText(activity, getString(R.string.error_message), Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val PARAM = "param"

        fun newInstance(url: String): DetailFragment {
            val fragment = DetailFragment()
            val args = Bundle()
            args.putString(PARAM, url)
            fragment.arguments = args
            return fragment
        }
    }
}

