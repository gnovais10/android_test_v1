package br.com.gustavonovais.githubapi.home

import br.com.gustavonovais.githubapi.models.RepoResponse

/**
 * Created by gustavon on 01/03/18.
 */

interface HomeView {
    fun getRepoSucess(dataRepoResponse: RepoResponse.DataRepoResponse)
    fun getRepoError()
}