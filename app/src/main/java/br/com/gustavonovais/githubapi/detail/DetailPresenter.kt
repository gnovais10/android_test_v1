package br.com.gustavonovais.githubapi.detail

import br.com.gustavonovais.githubapi.models.DetailResponse
import br.com.gustavonovais.githubapi.networking.NetworkError
import br.com.gustavonovais.githubapi.networking.Service
import java.util.ArrayList

/**
 * Created by gustavon on 06/03/18.
 */
class DetailPresenter(private var service: Service, private var view: DetailView) : Service.CallbackPull{
    override fun onSuccess(detailResponse: ArrayList<DetailResponse>) {
        view.getDetailSucess(detailResponse)
    }

    override fun onError(networkError: NetworkError?) {
        view.getDetailError()
    }

    fun getPulls(url: String){
        service.getPull(this, url)
    }

}