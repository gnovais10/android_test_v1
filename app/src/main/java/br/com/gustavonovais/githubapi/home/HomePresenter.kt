package br.com.gustavonovais.githubapi.home

import br.com.gustavonovais.githubapi.models.RepoResponse
import br.com.gustavonovais.githubapi.networking.NetworkError
import br.com.gustavonovais.githubapi.networking.Service

/**
 * Created by gustavon on 01/03/18.
 */
class HomePresenter(private var service: Service,private var view: HomeView) : Service.CallbackRepo {

    override fun onSuccess(dataRepoResponse: RepoResponse.DataRepoResponse) {
        view.getRepoSucess(dataRepoResponse)
    }

    override fun onError(networkError: NetworkError?) {
        view.getRepoError()
    }

    fun getRepo(repo : String, page: String){
        service.getRepo(this, repo, page)
    }

}