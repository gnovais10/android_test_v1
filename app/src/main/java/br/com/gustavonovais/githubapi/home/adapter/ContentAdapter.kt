package br.com.gustavonovais.githubapi.home.adapter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.gustavonovais.githubapi.R
import br.com.gustavonovais.githubapi.detail.DetailActivity
import br.com.gustavonovais.githubapi.home.HomeActivity.Companion.JAVA
import br.com.gustavonovais.githubapi.models.RepoResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_item.view.*

/**
 * Created by gustavon on 05/03/18.
 */
class ContentAdapter(private var listItem: ArrayList<RepoResponse.Item>,private var lastItemInterface: LastItemInterface): RecyclerView.Adapter<ContentAdapter.ViewHolder>() {

    private lateinit var activity : AppCompatActivity

    interface LastItemInterface {
        fun onLastItem(page: String, sizeList: Int)
    }


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = listItem.get(position)

        holder?.let {
            holder.nameAuthor.text = item.name
            holder.nameRepo.text = item.full_name
            holder.descRepo.text = item.description
            holder.numberFork.text = "forks: ${item.forks_count.toString()}"
            holder.numberStar.text = "stars: ${item.stargazers_count.toString()}"

            Picasso.with(activity).load(item.owner.avatar_url)
                    .placeholder(R.drawable.placeholder).into(holder.imageAuthor)

            holder.card_content.setOnClickListener {

                var intent  = Intent(activity, DetailActivity::class.java)
                intent.putExtra(DetailActivity.Companion.PARAM_URL, item?.pulls_url.substringBefore("{"))
                activity.startActivity(intent)
            }
        }

        if (position == listItem.size-1){
            lastItemInterface.onLastItem(JAVA, listItem.size)
            holder?.relativeContent?.visibility = View.GONE
            holder?.load?.visibility = View.VISIBLE
        } else {
            holder?.load?.visibility = View.GONE
            holder?.relativeContent?.visibility = View.VISIBLE
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.content_item, parent, false)
        activity = parent?.context as AppCompatActivity
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageAuthor = itemView.image_author
        val nameAuthor = itemView.name_author
        val nameRepo = itemView.name_repo
        val descRepo = itemView.desc_repo
        val numberFork = itemView.number_forks
        val numberStar = itemView.number_stars
        val relativeContent = itemView.relative_content
        val load = itemView.load
        val card_content = itemView.card_content
    }

}